class RSA{
	constructor(text = "")
	{
		this.text = text;
		this.public_key = 0;
		this.private_key = 0;
		this.n = 0;
	}

	setPublicKey(key, n)
	{
		this.public_key = BigInt(key);
		this.n = BigInt(n);
	}

	setPrivateKey(key, n)
	{
		this.private_key = BigInt(key);
		this.n = BigInt(n);
	}

	getPublicKey()
	{
		return this.public_key;
	}

	getPrivateKey()
	{
		return  this.private_key;
	}

	encrypt(text = "")
	{
		if (!(text)) {
			text = this.text ;
			if (!(text)) {
				return '';			
			}
		}
		if (!(this.public_key)) {
			console.log(this.public_key);
			return '';			
		}
		if (!(this.n)) {
			console.log(this.n);
			return '';			
		}
		let block = this.getBlock(text);
		let new_block = [];
		let new_value = BigInt(0);
		for (var i = 0; i < block.length; i++) {
			new_value = this.transformValue(BigInt(block[i]),this.public_key,this.n);
			new_block.push(new_value);
		}
		return new_block.join();
	}

	decrypt(text = ''){
		if (!(text)) {
			text = this.text ;
			if (!(text)) {
				return '';			
			}
		}
		if (!(this.private_key)) {
			return '';			
		}
		if (!(this.n)) {
			return '';			
		}
		let block = text.split(',');
		let new_block = [];
		let new_value = BigInt(0);
		for (var i = 0; i < block.length; i++) {
			new_value = this.transformValue(BigInt(block[i]),this.private_key,this.n);
			new_block.push(new_value);
		}
		text = this.getText(new_block);
		return text;
	}

	transformValue(value,key,n){
		return (value**key)%n ;
	}

	getBlock(text = ''){
		let block = [];
		if (!(text)) {
			return block;
		}
		let code = this.convertTextToCode(text);
		if (String(this.n).length > 5) {
			block = code.match(/.{1,5}/g);
		} else if (String(this.n).length > 4) {
			block = code.match(/.{1,4}/g);
		} else if (String(this.n).length > 3) {
			block = code.match(/.{1,3}/g);
		} else if (String(this.n).length > 2) {
			block = code.match(/.{1,2}/g);
		} else if (String(this.n).length > 1) {
			block = code.match(/.{1,1}/g);
		}
		return block;
	}

	getText(block = [])
	{
		let text = '';
		if (!(block)) {
			return text;
		}
		let size_block = 0;
		let code = '';
		if (String(this.n).length > 5) {
			size_block = 5
		} else if (String(this.n).length > 4) {
			size_block = 4
		} else if (String(this.n).length > 3) {
			size_block = 3;
		} else if (String(this.n).length > 2) {
			size_block = 2;
		} else if (String(this.n).length > 1) {
			size_block = 1;
		} else {
			return block;
		}
		for (var i = 0; i < block.length; i++) {
			let temp = '';
			if (i == block.length-1) {
				temp = block[i];
			} else {
				temp = ('00000' + block[i]).slice(-(size_block));
			}
			code += temp;
		}
		text = this.convertCodeToText(code);	
		return text;
	}

	convertTextToCode(text = ''){
		let code = '';
		for (var i = 0; i < text.length; i++) {
			code += ('000' + text.charCodeAt(i)).slice(-3);
		}
		return code;
	}

	convertCodeToText(code = ''){
		let text = '';
		let block_code = code.match(/.{1,3}/g);
		for (var i = 0; i < block_code.length; i++) {
			text += String.fromCharCode(block_code[i]);
		}
		return text;
	}

	
	// ============================================================
	//                          CODINGAN SAMPAH
	// ============================================================
	// getBlockFromText(text = ''){
	// 	let block = [];
	// 	if (!(text)) {
	// 		return block;
	// 	}
	// 	if (!(this.n)) {
	// 		return block;
	// 	}
	// 	/*------ begin script ------*/
	// 	// charset ASCII [0-127]
	// 	let code = this.convertTextToCode(text);
	// 	let temp = '';
	// 	let k = 0;
	// 	while(k < code.length){
	// 		temp += code[k];
	// 		if (
	// 			BigInt(temp) >= this.n 
	// 			|| temp.length > String(this.n).length
	// 		){
	// 			temp = temp.substring(0, temp.length-1);
	// 			block.push(BigInt(temp));
	// 			temp = '';
	// 		} else {
	// 			k++;
	// 		}
	// 	}
	// 	block.push(BigInt(temp));
	// 	console.log(block);

	// 	return block;
	// 	/*------ end script ------*/
	// }

	// getBlockFromTextForDecrypt(text = ''){
	// 	let block = [];
	// 	if (!(text)) {
	// 		return block;
	// 	}
	// 	if (!(this.n)) {
	// 		return block;
	// 	}
	// 	/*------ begin script ------*/
	// 	// charset ASCII [0-127]
	// 	let code = this.convertTextToCodeForDecrypt(text);
	// 	let temp = '';
	// 	let k = 0;
	// 	while(k < code.length){
	// 		temp += code[k];
	// 		if (
	// 			BigInt(temp) >= this.n 
	// 			|| temp.length > String(this.n).length
	// 		){
	// 			temp = temp.substring(0, temp.length-1);
	// 			block.push(BigInt(temp));
	// 			temp = '';
	// 		} else {
	// 			k++;
	// 		}
	// 	}
	// 	block.push(BigInt(temp));
	// 	console.log(block);

	// 	return block;
	// 	/*------ end script ------*/
	// }


	// getTextFromBlock(block){
	// 	let text = '';
	// 	if (!(block)) {
	// 		return text;
	// 	}
	// 	if (!(this.n)) {
	// 		return text;
	// 	}
	// 	/*------ begin script ------*/
	// 	// charset ASCII [0-127]
	// 	console.log(block);
	// 	let block_string = this.joinArrayToString(block);
	// 	console.log(block_string);
	// 	let temp = '';
	// 	let temp_text = []
	// 	let k = 0;
	// 	while(k < block_string.length){
	// 		temp += block_string[k];
	// 		if (
	// 			BigInt(temp) > BigInt(127) 
	// 			|| temp.length==String(127).length
	// 		){
	// 			temp = temp.substring(0, temp.length-1);
	// 			temp = parseInt(temp);
	// 			// if (BigInt(temp) < BigInt(127) && temp.length<String(127).length) {
	// 			// 	console.log('p  : 0'+String.fromCharCode(temp));
	// 			// 	temp_text.push('0'+String.fromCharCode(temp));
	// 			// } else {
	// 				// console.log('p  : '+String.fromCharCode(temp)+' '+temp);
	// 				temp_text.push(String.fromCharCode(temp));
	// 			// }
	// 			temp = '';
	// 		} else {
	// 			k++;
	// 		}
	// 	}
	// 	temp = parseInt(temp);
	// 	temp_text.push(String.fromCharCode(temp));
	// 	text = this.joinArrayToString(temp_text);
	// 	return text;
	// }

	// getTextFromBlockForDecrypt(block){
	// 	let text = '';
	// 	if (!(block)) {
	// 		return text;
	// 	}
	// 	if (!(this.n)) {
	// 		return text;
	// 	}
	// 	/*------ begin script ------*/
	// 	// charset ASCII [0-127]
	// 	console.log(block);
	// 	let block_string = this.joinArrayToStringForDecrypt(block);
	// 	console.log(block_string);
	// 	let temp = '';
	// 	let temp_text = []
	// 	let k = 0;
	// 	while(k < block_string.length){
	// 		temp += block_string[k];
	// 		if (
	// 			BigInt(temp) > BigInt(127) 
	// 			// || temp.length==String(127).length
	// 		){
	// 			temp = temp.substring(0, temp.length-1);
	// 			console.log('p : '+temp)
	// 			temp = parseInt(temp);
	// 				temp_text.push(String.fromCharCode(temp));
	// 			temp = '';
	// 		} else {
	// 			k++;
	// 		}
	// 	}
	// 	temp = parseInt(temp);
	// 	temp_text.push(String.fromCharCode(temp));
	// 	text = this.joinArrayToString(temp_text);
	// 	return text;
	// }

	// joinArrayToString(array){
	// 	let text = '';
	// 	for (var i = 0; i < array.length; i++) {
	// 		text += array[i];
	// 	}
	// 	return text;
	// }

	// joinArrayToStringForDecrypt(array){
	// 	let text = '';
	// 	for (var i = 0; i < array.length; i++) {
	// 		let temp = String(array[i]).length;
	// 		let sub = this.n/10n;
	// 		console.log(sub);
	// 		if (temp == String(this.n).length-1 && array[i] < sub) {
	// 			text = text+'0'+array[i];
	// 		} else {
	// 			text += array[i];
	// 		}
	// 	}
	// 	return text;
	// }

	// convertTextToCode(text)
	// {
	// 	let code = '';
	// 	for (var i = 0; i < text.length; i++) {
	// 		code += text.charCodeAt(i);
	// 	}
	// 	return code;
	// }

	// convertTextToCodeForDecrypt(text)
	// {
	// 	let code = '';
	// 	for (var i = 0; i < text.length; i++) {
	// 		let temp = String(text.charCodeAt(i)).length;
	// 		if (temp == String(127).length-2 & i!=text.length-1) {
	// 			code += '0'+text.charCodeAt(i);

	// 		} else {
	// 			code += text.charCodeAt(i);
	// 		}
	// 	}
	// 	console.log(code);
	// 	return code;
	// }

	// convertCodeToText(code = ''){
		// let text = '';
		// let i = 0;
		// let temp = '';
		// while(i < code.length){
		// 	temp += code.charAt(i);
		// 	if (BigInt(temp) > BigInt(127)) {
		// 		if (BigInt(temp) > BigInt(1270)) {
		// 			temp = temp.substring(0,temp.length-2);
		// 			console.log(temp);
		// 			text += String.fromCharCode(temp);
		// 			temp = '';
		// 			i--;
		// 		} else {
		// 			temp = temp.substring(0,temp.length-1);
		// 			console.log(temp);
		// 			text += String.fromCharCode(temp);
		// 			temp = '';
		// 		}
		// 	} else {
		// 		i++;
		// 	}
		// }
		// temp += code.charAt(i);
		// text += String.fromCharCode(temp);
		// return text;
	// }


}