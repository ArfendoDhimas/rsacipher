class generateKeyRSA {

	constructor() {
		this.privateKey = [];
		this.publicKey = [];
	}

	generateKey() {
		var p = this.getRandomNum();;
		while (!this.cekIsPrimeNumber(p)) {
			p = this.getRandomNum();
		}
		var q = this.getRandomNum();
		while ((p == q) || !this.cekIsPrimeNumber(q)) {
			q = this.getRandomNum();
		}

		var N = p * q;
		var phi_N = (p - 1) * (q - 1);

		var e = this.getRandomNum();
		while ((e > phi_N) || (this.getFPB(phi_N, e) != 1)) {
			e = this.getRandomNum();
		}

		var k = 1;
		var d = (1 + (k * phi_N)) / e;
		while (!this.cekIsWholeNumber(d)) {
			k++;
			d = (1 + (k * phi_N)) / e;
		}
		this.setPublicKey([e, N]);
		this.setPrivateKey([d, N]);
	}

	cekIsWholeNumber(num) {
		if (num == Math.floor(num)) {
			return true;
		} else {
			return false;
		}
	}

	getFPB(num1, num2) {
		if (num2 == 0) {
			return num1;
		} else {
			return this.getFPB(num2, num1 % num2);
		}
	}

	//seratus ribu sampai 1juta
	getRandomNum() {
		// dari banyak percobaan, algoritma di pdf munir hanya mampu rentang sekian
		return 50+(Math.floor(Math.random() * (150)));
	}

	cekIsPrimeNumber(num) {
		if (num < 2) {
			return false;
		}
		// nilai maksimal akar dari num
		var sqr_num = Math.sqrt(num);
		for (var i = 2; i < sqr_num; i++) {
			if ((num % i) == 0) {
				return false;
			}
		}
		return true;
	}

	setPublicKey(publicKey) {
		this.publicKey = publicKey;
	}

	getPublicKey() {
		return this.publicKey;
	}

	setPrivateKey(privateKey) {
		this.privateKey = privateKey;
	}
	getPrivateKey() {
		return this.privateKey;
	}
}